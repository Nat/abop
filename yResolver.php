<?php  

class yResolver
{	
	function get_openids($cid, $isXRI) {
		$url=$cid;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	  
		if(!$isXRI) {
			// Try XRDS First for URI
			// MUST set the application type. 
			curl_setopt($ch, CURLOPT_NOBODY, true);
			curl_setopt($ch, CURLOPT_HEADER, true);
			$header = curl_exec($ch);
			curl_setopt($ch, CURLOPT_NOBODY, false);
			$xrds = $this->get_xrds($header);
			if($xrds) {
					$err =  htmlspecialchars($xrds);
					$arr = $this->parse_xrds($xrds);
					if(!$arr['lid']) {
							$arr['lid']=$url;
					}
			} else {
				curl_setopt($ch, CURLOPT_NOBODY, false);
				$responseText = curl_exec($ch);
				$arr=$this->parse_html($responseText);
			}
		} else {
			$responseText = curl_exec($ch);
			$arr=$this->parse_html($responseText);
		}
		curl_close($ch);
		return $arr;
	}
	
	function get_xrds($header) {
			// parse header to get X-XRDS-Location: 
			$ha = explode("\n", $header);
			foreach($ha as $line) {
					if(preg_match('/X-XRDS-Location:/', $line) ) {
							$kv=explode(" ",$line,2);
							$xrds_loc = $kv[1];
							$isXRDS=true;
							break;
					}
			}
	
			// GET X-XRDS-Location
			if($isXRDS) {
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_NOBODY, false);
					curl_setopt($ch, CURLOPT_URL, $xrds_loc);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					$xrds = curl_exec($ch);
					curl_close($ch);
					return $xrds;
			} else {
					return false;
			}
	}
	
	function parse_xrds($xrds) {
			$xml = new SimpleXMLElement($xrds);
			$arr['ope'] = $xml->{'XRD'}->{'Service'}->{'URI'};
			$arr['lid'] = $xml->{'XRD'}->{'CanonicalID'};
			return $arr;
	}
	
	function parse_html($responseText) {
			$p = xml_parser_create();
			xml_parse_into_struct($p, $responseText, $vals, $index);
			xml_parser_free($p);
	
			$links=array();
	
			foreach($vals as $key => $val) {
				if($val['tag']=='LINK') {
					if( $val['attributes']['REL']=='openid2.provider') {
							$ope=$val['attributes']['HREF'];
					} elseif ($val['attributes']['REL']=='openid2.local_id') {
							$lid=$val['attributes']['HREF'];
					}
					if ($ope && $lid) {
							break;
					}
				}
			}
			$arr['ope']=$ope;
			$arr['lid']=$lid;
			return $arr;
	}
	
	function canonicalize($openid){
		$isXRI=false;
		if(preg_match('!http://xri.net/!',$openid)){
			$isXRI=true;
		}
		if(preg_match('/^[=@+$!\(]/',$openid)){
			if(preg_match('/[^=@+$!\(\w]/',$openid)) {
				$openid=urlencode($openid);
				$pat=array('%3D','%40','%2B','%24','%21','%28','%29','%2A');
				$rep=array('=','@','+','$','!','(',')','*');
				$openid=str_replace($pat,$rep,$openid);
			}
			$url='http://xri.net/'.$openid;
			$isXRI=true;
		} elseif(preg_match('/^xri:\/\//',$openid)) {
			$url = preg_replace('/^xri:\/\//','http://xri.net/',$openid);
			$isXRI=true;
		} elseif(preg_match('/^http[s]*/',$openid)) {
			$url = $openid;
		} else {
			$url = 'http://' . $openid;
		}
		$arr['isXRI']=$isXRI;
		$arr['cid']=$url;
		return $arr;
	}
}
?>
