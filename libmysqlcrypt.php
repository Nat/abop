<?php
/* 
 mysql_aes_encrypt/decript compatible Encryption
 Straight from /manual/en/ref.mcrypt.php
 
Usage: 
$val="For those looking for a mysql_aes_decrypt, I created this method, referencing rolf's aes_encrypt below. Since the aes_encrypt right-pads N * blocksize with any chr( 0 ) to chr( 16 ) (random based on the input string length) we first decrypt the text, then RTrim chr(0 .. 16) depending on its trailing ord() value.";

$ky="012345678901234567890";

$bEnc = mysql_aes_encrypt($val, $ky);
$plain = mysql_aes_decrypt($bEnc,$ky);

echo $plain;
*/

function mysql_aes_encrypt($val,$ky) {
    $mode=MCRYPT_MODE_ECB;    
    $enc=MCRYPT_RIJNDAEL_128;
    $val=str_pad($val, (16*(floor(strlen($val) / 16)+(strlen($val) % 16==0?2:1))), chr(16-(strlen($val) % 16)));
    return mcrypt_encrypt($enc, $ky, $val, $mode, mcrypt_create_iv( mcrypt_get_iv_size($enc, $mode), MCRYPT_DEV_URANDOM));
}

function mysql_aes_decrypt( $val, $ky ) {           
           $mode = MCRYPT_MODE_ECB; 
           $enc = MCRYPT_RIJNDAEL_128;
           $dec = @mcrypt_decrypt($enc, $ky, $val, $mode, @mcrypt_create_iv( @mcrypt_get_iv_size($enc, $mode), MCRYPT_DEV_URANDOM ) );
           return rtrim( $dec, ( ( ord(substr( $dec, strlen( $dec )-1, 1 )) >= 0 and ord(substr( $dec, strlen( $dec )-1, 1 ) ) <= 16 ) ? chr(ord(substr( $dec, strlen( $dec )-1, 1 ))): null) );
}
?>
