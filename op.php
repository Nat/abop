<?php
/**
 * op.php
 *
 * This is a sample implementation of OpenID/AB1.0 draft12 provider.
 * License: GPL v.3
 *
 * @author Nat Sakimura (http://www.sakimura.org/)
 * @version 0.6
 * @create 2010-06-12
 */

include_once("abconstants.php");
include_once("libjsoncrypto.php");

define("DEBUG",0);
define("OP_ENDPOINT","https://" . OP_SERVER_NAME . "/abop/op.php");
define("SERVER_ID","https://" . OP_SERVER_NAME . "/abop/");

define("AES_KEY","A_very_secret_key_it_is");
header('Content-Type: text/html; charset=utf8');

session_start();
include_once("libmysqlcrypt.php");
//include_once("libmagicsignatures.php");
$mode=$_REQUEST['mode'];

if(!$mode) {
    if($_REQUEST['response_type'] == 'code' && isset($_REQUEST['request_uri']))
        $mode = "art_req";
    elseif($_REQUEST['grant_type'] == 'authorization_code' && isset($_REQUEST['code']) && isset($_REQUEST['client_id']))
        $mode = "direct_assertion_req";
}

if($mode=="art_req") {
    $_SESSION['get']=$_GET;
    $request_uri=$_REQUEST['request_uri'];
    $_SESSION['request_uri']=$request_uri;
    if($_SESSION['username']) {
        $rpf=get_url($request_uri);
        $rpfA=json_decode($rpf);
        if(!$rpfA) {
                echo "<h2>Error: Malformed RPF</h2>";
                echo "<p>Following is not a valid JSON expression.</p>";
                echo "<p>rurl:" . $request_uri. "</p>";
                echo var_dump($_SESSION);
                echo "<pre>" . $rpf . "</pre>";
                exit;
        }
        $_SESSION['rpfA']=$rpfA;
        if($_GET['state'])
            $rpfA->{'state'} = $_GET['state'];
        if ($rpfA->{'openid'}->{'ns.ax'}) {
                echo ax_confirm($rpf);
                exit();
        }
    } else {
        echo loginform();
    }
} elseif ($mode=="pop") {
    // check Proof of Posession (pop) 
    $pop=0;
    $username=preg_replace('/[^\w=_@]/','_',$_POST['username']);
    if(check_credential($username,$_POST['password'])){
	$_SESSION['login']=1;
	$_SESSION['username']=$username;
	$_SESSION['persist']=$_POST['persist'];
        //  var_dump($_SESSION);
        $GET=$_SESSION['get'];
        $request_uri=$GET['request_uri'];
        $rpf=get_url($request_uri);
        $rpfA=json_decode($rpf);
	if(!$rpfA) {
		echo "<h2>Error: Malformed RPF</h2>";
		echo "<p>Following is not a valid JSON expression.</p>";
		echo "<p>rurl:" . $request_uri. "</p>";
		echo var_dump($_SESSION);
		echo "<pre>" . $rpf . "</pre>";
		exit;
	}
	$_SESSION['rpfA']=$rpfA;
	if ($rpfA->{'openid'}->{'ns.ax'}) {
		echo ax_confirm($rpf);
		exit();
	}
        $rpep=$rpfA->{'redirect_uri'};
	$atype = $rpfA->{'openid'}->{'atype'};
	$confirmed_attribute_list = array();
	foreach($_POST as $key => $value) {
 	    if(strncasecmp($key, "ax_", 3) == 0) {
 	        array_push($confirmed_attribute_list, 'ax.' . substr($key,3));
 	    }
 	}
 	$confirmed_attribute_list = implode(chr(0), $confirmed_attribute_list);

	$code=makeCode($_POST['username'],$request_uri,AES_KEY,0,$atype, $POST['persona'], $confirmed_attribute_list );
        $url="$rpep?code=$code&server_id=" . SERVER_ID . "";
	$state=$GET['state'];
	if($state){
		$url .= '&state='. $state;
	}
	//echo "<pre>".$url."</pre>";
	if($_SESSION['persist']=='on'){
		clean_session();
		$_SESSION['lastlogin']=time();
		$_SESSION['username']=$_POST['username'];
		$_SESSION['persist']=='on';
	} else {
        	session_destroy();
	}
        header("Location:$url");
    } else { // Credential did not match so try again. 
	echo loginform();
    }
} elseif ($mode=="direct_assertion_req") {
   // deCode 'code' to get some state information
    $enc=$_GET['enc'];
    $acode=deCode($_GET['code'],AES_KEY);
    if(!$acode) {
		$ea['error']="Invalid Code";
		echo json_encode($ea);
       	 	exit;
    }
    // print_r($acode);

    // Check If the 'code' is expired (60 secs). 
/*    
    if(time() > $acode['e']) {
	// [Todo] Expired, so return negative assertion. 
	echo '{"error":"Requirest Expired.","mode":"id_res"}';
    }
*/
    // obtain identity and request json from it. 
    $fname = "ids/".$acode['u']. ($acode['s'] ? ".{$acode['s']}" : '') . '.json';
    if(file_exists($fname)) {
	    $idfile = file_get_contents($fname);
    } 
    $identity = json_decode($idfile,true);
    $request  = json_decode(get_url($acode['r']),true);
    if(DEBUG) {
    	echo "<br />rurl: " . $acode['r'];
    	echo "<br />req : " . var_dump($request);
    }

    // Check if client_id (and secret) matches that of request file. 
    if($request['client_id']!=$_REQUEST['client_id']) {
	$errStr='{"error":"invalid_client", 
		"error_description":"' 
	. $request['openid']['client_id'] . '!=' . $_REQUEST['client_id'] 
	. '"}';
	echo $errStr;
	exit;
    }
    // [Todo] Implement client secret check. 

    // Create Assertion by intersect operation. 
    // if($acode['c']==0) then remove ax. keys. 
    if($acode['c']==0){
            $request = json_decode(file_get_contents('ids/assertion.json'),true);
    }else {
        $attribute_list = explode(chr(0), $acode['l']);
        foreach($identity['openid'] as $key => $value) {
            if(strncasecmp($key, 'ax.', 3) == 0) {
                if(!in_array($key, $attribute_list)) {
                    if(strcasecmp($key, 'ax.mode') != 0)
                        unset($identity['openid'][$key]);
                }
            }
        }
    }

    $assertion = array_intersect_key ($identity,$request) ;
    // add op_endpoint
    $assertion['openid']['op_endpoint']=OP_ENDPOINT; 

    // add back ax.update_url
    if($request['ax.update_url']) {
	$assertion['ax.update_url'] = $request['ax.update_url'];
    }
    // [Todo] PAPE Support. 
    $assertion['openid']['pape']=$request['pape.preferred_auth_policies'];
    if($request['pape.preferred_auth_policies']
        == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/privatepersonalidentifier") {
	if($request['client_id']) {
            $client_id = $request['client_id'];
        } elseif($request['realm']) {
            $client_id = $request['realm'];
        } else {
            $client_id = $request['redirect_to'];
        }
        $client_id = base64url_encode(md5($client_id,true));
	$str = makeCode($acode['u'],$client_id,AES_KEY);
        $assertion['openid']['claimed_id']=SERVER_ID.$str;
        $assertion['openid']['identity']=SERVER_ID.$str;
	$assertion['openid']["ns.pape"]="http://specs.openid.net/extensions/pape/1.0";
        $assertion['openid']["pape.preferred_auth_policies"]="http://schemas.xmlsoap.org/ws/2005/05/identity/claims/privatepersonalidentifier";
    }

    // add various variables for asserion
    $assertion['openid']['user_id']=SERVER_ID.$acode['u'];
    $assertion['openid']['server_id']=SERVER_ID;
    $assertion['openid']['client_id']=$request['client_id'];
    $assertion['openid']['request_uri']=$acode['r'];
    $assertion['openid']['issued_at']=time();
    $assertion['openid']['expires_in']=3600;
    $assertion['openid']['type']="http://openid.net/specs/ab/1.0#id_res";
    $assertion['openid']['refresh_uri']=array(SERVER_ID . 'refresh');
    $assertion['openid']['state']=$request['state'];
    
    $assertion['openid']['pubkey'] = base64url_encode(pem2der(file_get_contents(OP_PCERT)));
    unset($assertion['openid']['cd:sha1pass']);
    // For OpenID Connect
    $cdata = $assertion['access_token'] 
     . $assertion['expires_in'] . $assertion['issued_at'] 
     . $assertion['user_id']; 
    $csecret = get_client_secret($assertion['client_id']);
    $assertion['signature']=hash_hmac('sha256',$cdata,$csecret,false);

    // Check if the request is for JSONP
    if($acode['t']==1) {
	$tjsonp=1;
    }
    if($_GET['atype']=="jsonp"){
        $tjsonp=1;
    }
    if($tjsonp) {
	header("content-type: application/json");
	echo "openidjsonp(";
    }
    // If Signed format is required, magic_sign. Otherwise, return assertion.
    if(DEBUG) {print_r($acode);}
    if($acode['t']==2 || $acode['t'] == 6 || $enc) {
    	$pkeyfile=OP_PKEY;
//    	echo magic_sign(json_encode($assertion), "application/json", $pkeyfile, "","http://openid.net/specs/ab/1.0#signed_format", "https://" . OP_SERVER_NAME . "/abop/");
        $sig_params = array(
                          array('key_id' => RP_SERVER_NAME, 'algorithm' => 'HMAC-SHA256'), 
                          array('certs_uri' => OP_PCERT_URL, 'algorithm' => 'RSA-SHA256')
                       );
        $sig_keys = array("aaa", array('key_file' => OP_PKEY, 'password' => OP_PKEY_PASSPHRASE));
        
        $signed_json = json_simple_sign($assertion, $sig_params, $sig_keys);
        
        if($acode['t'] == 2)
            echo $signed_json;
        elseif($acode['t'] == 6 || $enc) {
            
            $encrypted_json = js_encrypt($signed_json, der2pem(base64url_decode($request['openid']['pubkey'])), false, NULL, RP_PCERT_URL, $request['openid']['enc_key'], $request['openid']['enc_type']);
            echo json_encode($encrypted_json);
        }
    } else {
	echo json_encode($assertion);
    }
    if($tjsonp){
	echo ");";
    }
} elseif ($mode=="ax_confirm") {
    if(DEBUG) {
    	echo "<pre>"; var_dump($_SESSION['rpfA']); echo "</pre>";
    	echo "<pre>"; var_dump($_POST); echo "</pre>";
    }
    $GET=$_SESSION['get'];
    $rpfA=$_SESSION['rpfA'];
    $rpep=$rpfA->{'redirect_uri'};
    $atype=$rpfA->{'openid'}->{'atype'};
    if(DEBUG) {
	echo "atype:" . $atype;
    }


    if ($_REQUEST['agreed']=="1") {
      	$confirmed_attribute_list = array();
      	foreach($_POST as $key => $value) {
       	    if(strncasecmp($key, "ax_", 3) == 0) {
       	        array_push($confirmed_attribute_list, 'ax.' . substr($key, 3));
       	    }
       	}
       	$confirmed_attribute_list = implode(chr(0), $confirmed_attribute_list);
        $code=makeCode($_SESSION['username'],$_SESSION['request_uri'],AES_KEY,1,$atype, $_POST['persona'], $confirmed_attribute_list );
    } else {
        $code=makeCode($_SESSION['username'],$_SESSION['request_uri'],AES_KEY,0,$atype );
    }
    $url="$rpep?code=$code&server_id=" . SERVER_ID . "";
   	$state=$GET['state'];
   	if($state){
   		$url .= '&state='. $state;
   	}
        if($_SESSION['persist']=='on'){
		$username = $_SESSION['username'];
                clean_session();
                $_SESSION['lastlogin']=time();
                $_SESSION['username']=$username;
		$_SESSION['persist']=='on';
        } else {
                session_destroy();
        }
        

    header("Location:$url");
}


/**
 * Read the identity file and compair password. 
 * @param  String $username   Local ID of the user. 
 * @param  String $password   User input password.
 * @return String true if OK, else false. 
 */
function check_credential($username, $password) {
    $filename = "ids/" . $username . '.json';
    if(file_exists($filename)) {
	$jdentity = file_get_contents($filename);
	$arr = json_decode($jdentity,1);
	$sha1p = sha1($password);
	$cred = $arr["openid"]["cd:sha1pass"];
	if($sha1p==$cred){
		return 1;
	} else {
		return 0;
	}
    } else {
	echo $filename;
	return 0;
    }
}

/**
 * Show Login form. 
 * @return String HTML Login form. 
 */
function loginform(){
	 $str='
	<html>
	<head><title>' . OP_SERVER_NAME . ' OP</title>
	<meta name="viewport" content="width=320">
	</head>
	<body style="background-color:#FFEEEE;">
	<h1>' . OP_SERVER_NAME . ' OP Login</h1>
	<form method="POST" action="">
	Username:<input type="text" name="username" value="=alice">(or =bob)<br />
	Password:<input type="password" name="password" value="wonderland">(or underland)<br />
	<input type="hidden" name="mode" value="pop"><br />
	<input type="checkbox" name="persist">Keep me logged in. <br />
	<input type="submit">
	';
	return $str;
}

/**
 * Show Confirmation Dialogue for Attributes. 
 * @param  String $r     Request String (JSON) 
 * @return String HTML to be shown. 
 */
function ax_confirm($r){
	$sreq="";
  $rpfA=json_decode($r,true);
	$axlabel=json_decode(file_get_contents("ids/ax.json"),true);
	$rl = array_intersect_key($axlabel,$rpfA['openid']);


  $tab_headers = array();
  $tabs = array();
  $filenames = glob('ids/' . $_SESSION['username'] . '.*json');
  $i = 0;
  foreach($filenames as $filename) {
    ++$i;
    $path_parts = pathinfo($filename);
    $name_parts = explode('.', $path_parts['filename']);
    $persona = $name_parts[1];
    $persona_ui = ucfirst($name_parts[1]);
    if(!$persona) {
        $persona_ui = 'Default';
        $persona = '';
    }
    array_push($tab_headers, "<li><a href='#tabs-$i'>$persona_ui</a></li>");
        
    if(file_exists($filename)) {
        $idfile = file_get_contents($filename);
    } 
    $identity = json_decode($idfile,true);
    foreach($rl as $key => $value) {
        $sreq .= "<li>$value</li>\n";
        $attribs[$key] = $identity['openid'][$key];
    }

    $attributes = NULL;
	  foreach($attribs as $key => $value) {
	      $attributes .= "<tr><td>{$rl[$key]}</td><td>{$attribs[$key]}</td><td><input type='checkbox' name='$key' value='1' checked></td></tr>\n";
	  }
	  
$persona_form_template = <<<EOF
  <div id='tabs-$i'>
	<form method="POST" action="">
	<input type="hidden" name="mode" value="ax_confirm">
	<input type="hidden" name="persona" value="$persona">
	<table cellspacing="0" cellpadding="0" width="600">
	<thead><tr><th>Attribute</th><th>Value</th><th>Confirm</th></tr></thead>
	$attributes
	<tr><td colspan="3">&nbsp;</td></tr>
	<tr><td colspan="3"><input type="checkbox" name="agreed" value="1" checked>&nbsp;&nbsp;&nbsp;
	Agree to Provide the above selected attributes.</td></tr>
	<tr><td colspan="3"><input type="submit" name="confirm" value="confirmed"></td></tr></table>
	</form>
	</div>
EOF;

    array_push($tabs, $persona_form_template);
        

  }

  $fname = "ids/".$_SESSION['username']. '.json';
  if(file_exists($fname)) {
	  $idfile = file_get_contents($fname);
  } 
  $identity = json_decode($idfile,true);
	foreach($rl as $key => $value) {
		$sreq .= "<li>$value</li>\n";
		$attribs[$key] = $identity['openid'][$key];
	}
        if(DEBUG){
		echo "<pre>";
		echo "<h4>rpfA</h4>";
		print_r($rpfA);
		echo "<h4>axlabel</h4>";
		print_r($axlabel);
		echo "<h4>intersect</h4>";
		print_r($rl);


		echo "<h4>session</h4>";
		print_r($_SESSION);
		
		
		echo "<h4>identity assertion</h4>";
    print_r($identity);
		
		echo "<h4>return attributes</h4>";
	  print_r($attribs);
		
		echo "</pre>";
		
		
	}
	
	$attributes = '';
	foreach($attribs as $key => $value) {
	    $attributes .= "<tr><td>{$rl[$key]}</td><td>{$attribs[$key]}</td><td><input type='checkbox' name='$key' value='1' checked></td></tr>\n";
	}
	
$jquery = <<<EOF
		<link type="text/css" href="css/smoothness/jquery-ui-1.8.6.custom.css" rel="stylesheet" />	
		<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
		<script type="text/javascript" src="js/jquery-ui-1.8.6.custom.min.js"></script>
		<script type="text/javascript">
			$(function(){
	
				// Tabs
				$('#tabs').tabs();
				
				//hover states on the static widgets
				$('#dialog_link, ul#icons li').hover(
					function() { $(this).addClass('ui-state-hover'); }, 
					function() { $(this).removeClass('ui-state-hover'); }
				);
				
			});
		</script>

		<style type="text/css">
			/*demo page css*/
			body{ font: 62.5% "Trebuchet MS", sans-serif; margin: 50px;}
			.demoHeaders { margin-top: 2em; }
			#dialog_link {padding: .4em 1em .4em 20px;text-decoration: none;position: relative;}
			#dialog_link span.ui-icon {margin: 0 5px 0 0;position: absolute;left: .2em;top: 50%;margin-top: -8px;}
			ul#icons {margin: 0; padding: 0;}
			ul#icons li {margin: 2px; position: relative; padding: 4px 0; cursor: pointer; float: left;  list-style: none;}
			ul#icons span.ui-icon {float: left; margin: 0 4px;}
		</style>	
EOF;

$headers = implode("\n", $tab_headers);
$personas = implode("\n", $tabs);
$tabs = <<<EOF
		<h2 class="demoHeaders">Personas</h2>
		<div id="tabs" style='width:640'>
			<ul>
				$headers
			</ul>
			$personas

		</div>

EOF;
	
	$str= '
	<html>
	<head><title>' . OP_SERVER_NAME . ' AX Confirm</title>
	<meta name="viewport" content="width=620">' . $jquery . '
	</head>
	<body style="background-color:#FFEEEE;">
	<h1>' . OP_SERVER_NAME . ' AX Confirm</h1>
	<h2>RP requests following AX values...</h2>' . $tabs . '
	</body>
	</html>
	';
	return $str;
}


    
/**
 * Make Random String of a desired length. 
 * @param  String $length  Length of the random text. 
 * @return String Random text. 
 */
function makePin($length=8) { 
    // makes a random alpha numeric string of a given lenth 
    $str=mcrypt_create_iv(intval($length/1.3),MCRYPT_DEV_URANDOM);
    return base64url_encode($str) ;
} 

/**
 * Create a 'code' that has necessary information in it. 
 * @param  String $uname   Username
 * @param  String $rurl    Request URL
 * @param  String $password  Encryption Key.
 * @param  String $confirm
 * @param  String $atype
 * @return String Artifact Code. 
 */
function makeCode($uname,$rurl,$password, $confirm=0, $atype="openid2json", $persona=NULL, $attribute_list=NULL) {
    $password=md5($password);
    $expires_in = 60; //in seconds
    $arr['e'] = time()+ $expires_in;
    $arr['u'] = $uname;
    $arr['r'] = $rurl;
    //    echo "<h1>atype:".$atype."</h1>";
    if($atype=="openid2json" ) {
	$atype=0;
    } elseif ($atype=="openid2jsonp") {
	$atype=1;
    } elseif($atype=='openid2json+sig') {
	$atype=2;
    } elseif ($atype=="openid2json+sig+enc") {
	$atype=6;
    } else {
	$atype=0;
    }
    $arr['t'] = $atype; // 0=json, 1=jsonp, 2=json+sign, 6=json+sign+enc
                        // 8=saml, 16=wss
    $arr['p'] = makePin(4);
    $arr['s'] = $persona;
    $arr['l'] = $attribute_list;
    $arr['c'] = $confirm; // 1=ax confirmed. 
    $jstr = json_encode($arr);
    $method = "AES-128-CBC";
    $bstr = mysql_aes_encrypt($jstr,$password);
    $code = base64url_encode($bstr,1,0);
    return $code;
}

/**
 * Decode the 'code' to obtain the JSON. 
 * @param  String $code      'code' to be decoded. 
 * @return Mixed    Decoded JSON as an Array. OR false. 
 */
function deCode($code,$password) {
    $password=md5($password);
    $bstr = base64url_decode($code);
    $jstr = mysql_aes_decrypt($bstr,$password);
    $arr = json_decode($jstr,true);
    if($arr) {
        return $arr;
    } else {
        return false;
    }
}

/**
 * Obtain the content of the URL. 
 * @param  String $url      URL from which the content should be obtained. 
 * @return String Response Text. 
 */
function get_url($url) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $responseText = curl_exec($ch);
    curl_close($ch);
    return $responseText;
}

/**
 * Clean up the SESSION variables. 
 * @param String $persist  Whether to persist the login session
 * @return Int 1 if success. 0 if error. 
 */
function clean_session($persist=0){
	unset($_SESSION['get']);
	unset($_SESSION['request_uri']);
	unset($_SESSION['rpfA']);
	if(!$persist){
		unset($_SESSION['login']);
		unset($_SESSION['username']);
		unset($_SESSION['persist']);
	}
	return true;
}

/**
 * Get Client Secret stub
 * @param String $client_id
 * @return String Client Secret
 */
function get_client_secret($client_id) {
	$sec['https://' . RP_SERVER_NAME . '/abrp/']='shared_secret_is_not_so_secure';
	$secret=$sec[$cleint_id];
	if ($secret=='') {
		$secret='shared_secret_is_not_so_secure';
        }
	return $secret;
}
?>
